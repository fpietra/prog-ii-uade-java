package principal;

import implementaciones.*;
import algoritmos.Metodos;

public class Main {

	public static void main(String[] args) {
		Metodos m=new Metodos();
		ColaPI cola=new ColaPI();
		ColaPI cola2=new ColaPI();
		cola.inicializarCola();
		cola2.inicializarCola();
		cola.Acolar(1);
		cola.Acolar(2);
		cola.Acolar(3);
		cola.Acolar(4);
		cola.Acolar(5);
		//cola2.Acolar(7);
		//cola2.Acolar(3);
		//cola2.Acolar(8);
		//m.CopiarCola(cola, cola2);
		//m.ImprimirCola(cola);
		//m.ImprimirCola(cola2);
		//boolean capi=m.UltimoIgual(cola, cola2);		
		//System.out.println(capi);
		//m.ImprimirCola(cola);
		//boolean capi=m.EsCapicua(cola);
		//System.out.print(capi);
		//m.ImprimirCola(cola);
		DicMultipleA dic = new DicMultipleA();
		dic.InicializarDiccionario();;
		dic.Agregar(1, 2);
		dic.Agregar(1, 12);
		dic.Agregar(3, 4);
		dic.Agregar(3, 14);
		dic.Agregar(5, 6);
		dic.Agregar(5, 16);
		//m.MostrarDiccionarioMultiple(dic);
		
		ConjuntoTA conj1=new ConjuntoTA();
		ConjuntoTA conj2=new ConjuntoTA();
		ConjuntoTA inter=new ConjuntoTA();
		conj1.InicializarConjunto();
		conj2.InicializarConjunto();
		inter.InicializarConjunto();
		
		conj1.Agregar(1);
		conj1.Agregar(2);
		conj1.Agregar(3);
		conj1.Agregar(4);
		conj1.Agregar(5);
		conj1.Agregar(6);
		conj2.Agregar(2);
		conj2.Agregar(15);
		conj2.Agregar(4);
		conj2.Agregar(27);
		conj2.Agregar(6);
		conj2.Agregar(67);
		conj2.Agregar(132);
		
		inter=m.Resta(conj1, conj2);
		m.MostrarConjunto(inter);
		
	}

}
