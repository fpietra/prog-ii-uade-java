package algoritmos;

import implementaciones.*;
import interfaces.*;

public class Metodos {
	
	//METODOS PARA PILAS
	public void PasarPila(PilaTDA o, PilaTDA d){
		while(!o.PilaVacia()){
			d.Apilar(o.Tope());
			o.Desapilar();			
		}		
	}
	
	public void MostrarPila(PilaTDA o){
		PilaTF aux=new PilaTF();
		aux.InicializarPila();
		if (!o.PilaVacia()){
			while(!o.PilaVacia()){
				System.out.println(o.Tope());
				aux.Apilar(o.Tope());
				o.Desapilar();
			}
			PasarPila(aux,o);
		}
		else
			System.out.println("Pila Vacia");
	}
	
	public void InvertirPila(PilaTDA p) {
		PilaTF aux=new PilaTF();
		PilaTF aux2=new PilaTF();
		aux.InicializarPila();
		aux2.InicializarPila();

		PasarPila(p,aux);
		PasarPila(aux,aux2);
		PasarPila(aux2,p);
	}
	
	public void CopiarPila(PilaTDA o, PilaTDA d){
		PilaTF aux=new PilaTF();
		aux.InicializarPila();
		PasarPila(o,aux);
		while(!aux.PilaVacia()){
			d.Apilar(aux.Tope());
			o.Apilar(aux.Tope());
			aux.Desapilar();
		}		
	}
	
	public int ContarPila(PilaTDA p) {
		int cant=0;
		PilaTF aux=new PilaTF();
		aux.InicializarPila();
		
		CopiarPila(p,aux);
		while(!aux.PilaVacia()) {
			cant++;
			aux.Desapilar();
		}
		return cant;
	}
	
	public int SumarPila(PilaTDA p) {
		int suma=0;
		PilaTF aux=new PilaTF();
		aux.InicializarPila();
		
		CopiarPila(p,aux);
		while(!aux.PilaVacia()) {
			suma+=aux.Tope();
			aux.Desapilar();
		}
		return suma;
	}
	
	public float PromedioPila(PilaTDA p) {
		int suma=SumarPila(p);
		int cant=ContarPila(p);
		float prom=suma/cant;
		
		return prom;
	}
	
	
	//METODOS PARA COLAS
	public void PasarCola(ColaTDA o, ColaTDA d) {
		while(!o.ColaVacia()) {
			d.Acolar(o.Primero());
			o.Desacolar();
		}
	}
	
	public void CopiarCola(ColaTDA o, ColaTDA c) {
		ColaPI aux=new ColaPI();
		aux.inicializarCola();
		PasarCola(o,aux);
		while(!aux.ColaVacia()) {
			c.Acolar(aux.Primero());
			o.Acolar(aux.Primero());
			aux.Desacolar();
		}
	}
	
	public void InvertirColaAux(ColaTDA o) {
		PilaTF aux=new PilaTF();
		aux.InicializarPila();
		
		while(!o.ColaVacia()) {
			aux.Apilar(o.Primero());
			o.Desacolar();
		}
		while(!aux.PilaVacia()) {
			o.Acolar(aux.Tope());
			aux.Desapilar();
		}
	}
	
	public void InvertirCola(ColaTDA c) {
		if(!c.ColaVacia()) {
			int primero=c.Primero();
			c.Desacolar();
			InvertirCola(c);
			c.Acolar(primero);
		}
	}
	
	public void ImprimirCola(ColaTDA o) {
		ColaPI aux=new ColaPI();
		aux.inicializarCola();
		CopiarCola(o,aux);
		while(!aux.ColaVacia()) {
			System.out.print(aux.Primero());
			aux.Desacolar();
		}
		System.out.println();
	}
	
	public boolean UltimoIgual(ColaTDA c1,ColaTDA c2) {
		boolean iguales=false;
		
		InvertirCola(c1);
		InvertirCola(c2);
		if(c1.Primero()==c2.Primero())
			iguales=true;
		InvertirCola(c1);
		InvertirCola(c2);
		
		return iguales;
	}
	
	public boolean EsCapicua(ColaTDA c) {
		boolean capicua=true;
		ColaPI aux=new ColaPI();
		ColaPI aux2=new ColaPI();
		aux.inicializarCola();
		aux2.inicializarCola();
		
		CopiarCola(c, aux);
		CopiarCola(c, aux2);
		InvertirCola(aux);
		
		while(!aux.ColaVacia()) {
			if(aux.Primero()!=aux2.Primero())
				capicua= false;
			break;
		}
		
		return capicua;
		
	}
	
	public boolean EsInversa(ColaTDA c1, ColaTDA c2) {
		ColaPI aux=new ColaPI();
		aux.inicializarCola();
		CopiarCola(c2,aux);
		InvertirCola(aux);
		while(!aux.ColaVacia() || !c1.ColaVacia()) {
			if(aux.Primero() != c1.Primero())
				return false;
			aux.Desacolar();
			c1.Desacolar();
		}
		return true;
	}
	
	//METODOS PARA CONJUNTOS
	public ConjuntoTA Union(ConjuntoTA c1, ConjuntoTA c2) {
		int elem=0;
		ConjuntoTA unido = new ConjuntoTA();
		ConjuntoTA aux = new ConjuntoTA();
		
		unido.InicializarConjunto();
		aux.InicializarConjunto();
		
		CopiarConjunto(c1,unido);
		CopiarConjunto(c2,aux);

		while(!aux.ConjuntoVacio()) {
			elem=aux.Elegir();
			if(!unido.Pertenece(elem))
				unido.Agregar(elem);		
			aux.Sacar(elem);
		}		
		return unido;
	}
	
	public ConjuntoTA Interseccion(ConjuntoTA c1, ConjuntoTA c2) {
		int elem;
		ConjuntoTA aux1 = new ConjuntoTA();
		ConjuntoTA inter = new ConjuntoTA();
		aux1.InicializarConjunto();
		inter.InicializarConjunto();
		
		CopiarConjunto(c1,aux1);
		
		while(!aux1.ConjuntoVacio()) {
			elem=aux1.Elegir();
			if(c2.Pertenece(elem)) {
				inter.Agregar(elem);
			}
			aux1.Sacar(elem);
		}
		return inter;
	}
	
	public ConjuntoTA Resta(ConjuntoTA c1, ConjuntoTA c2) {
		ConjuntoTA resultado = new ConjuntoTA();
		ConjuntoTA aux = new ConjuntoTA();

		resultado.InicializarConjunto();
		aux.InicializarConjunto();
		
		CopiarConjunto(c1,resultado);
		CopiarConjunto(c2,aux);
		
		while(!aux.ConjuntoVacio()) {
			int elem = aux.Elegir();
			if(resultado.Pertenece(elem))
				resultado.Sacar(elem);
			aux.Sacar(elem);
		}
		return resultado;
	}
	
	public void PasarConjunto(ConjuntoTA c1, ConjuntoTA c2) {
		int elem=0;
		while(!c1.ConjuntoVacio()) {
			elem=c1.Elegir();
			c2.Agregar(elem);
			c1.Sacar(elem);
		}
	}
	
	public void CopiarConjunto(ConjuntoTA c1, ConjuntoTA c2) {
		ConjuntoTA aux=new ConjuntoTA();
		int elem=0;
		aux.InicializarConjunto();
		PasarConjunto(c1, aux);
		while(!aux.ConjuntoVacio()) {
			elem=aux.Elegir();
			c1.Agregar(elem);
			c2.Agregar(elem);
			aux.Sacar(elem);
		}
	}
	
	public void MostrarConjunto(ConjuntoTA c) {
		ConjuntoTA aux = new ConjuntoTA();
		aux.InicializarConjunto();
		
		CopiarConjunto(c,aux);
		
		while(!aux.ConjuntoVacio()) {
			int elem=aux.Elegir();
			System.out.println(elem);
			aux.Sacar(elem);
		}
	}
	
	//METODOS PARA DICCIONARIOS SIMPLES
	public void MostrarDiccionarioSimple(DiccionarioSimpleTDA d) {
		ConjuntoTA claves=new ConjuntoTA();
		int clave=0;
		claves.InicializarConjunto();
		claves=(ConjuntoTA) d.Claves();
		while(!claves.ConjuntoVacio()) {
			clave=claves.Elegir();
			System.out.println("Clave: " +clave+" - valor: " + d.Recuperar(clave));
			claves.Sacar(clave);
		}
		
	}
	
	//METODOS PARA DICCIONARIOS MULTIPLES
	public void MostrarDiccionarioMultiple(DiccionarioMultipleTDA d) {
		ConjuntoTA claves=new ConjuntoTA();
		int clave=0;
		int valor=0;
		ConjuntoTA valores=new ConjuntoTA();
		claves.InicializarConjunto();
		valores.InicializarConjunto();
		claves=(ConjuntoTA) d.Claves();
		while(!claves.ConjuntoVacio()) {
			clave=claves.Elegir();
			System.out.print("Clave: "+clave+" - valores: ");
			valores=(ConjuntoTA) d.Recuperar(clave);
			while(!valores.ConjuntoVacio()) {
				valor=valores.Elegir();
				System.out.print(valor + ", ");
				valores.Sacar(valor);
			}
			System.out.println();
			claves.Sacar(clave);
		}
		
	}
	
}
