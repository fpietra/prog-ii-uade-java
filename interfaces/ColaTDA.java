package interfaces;

public interface ColaTDA {
	void inicializarCola();
	
	void Acolar(int x);
	
	void Desacolar();
	
	boolean ColaVacia();
	
	int Primero();

}
