package implementaciones;

import interfaces.ColaTDA;

public class ColaPI implements ColaTDA{
	int[] a;
	int indice;

	@Override
	public void Acolar(int x) {
		a[indice]=x;
		indice++;
		
	}

	@Override
	public void Desacolar() {
		for(int i=0;i<indice-1;i++)
			a[i]=a[i+1];
		indice--;
	}

	@Override
	public boolean ColaVacia() {
		// TODO Auto-generated method stub
		return (indice==0);
	}

	@Override
	public int Primero() {
		// TODO Auto-generated method stub
		return a[0];
	}

	@Override
	public void inicializarCola() {
		// TODO Auto-generated method stub
		a=new int[100];
		indice=0;
	}

}
