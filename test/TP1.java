package test;

import algoritmos.Metodos;
import implementaciones.*;

public class TP1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		PilaTF p1=new PilaTF();// instancio la clase PilaTF en un objeto llamado p1
//		p1.InicializarPila(); // inicializo el objeto
//		p1.Apilar(2);
//		p1.Apilar(1);
//		PilaTF p2=new PilaTF();// instancio la clase PilaTF en un objeto llamado p1
//		p2.InicializarPila();
		Metodos m=new Metodos();
//		System.out.println("Muestro la pila 1");
//		m.MostrarPila(p1);
//		System.out.println("Muestro la pila p2");
//		m.MostrarPila(p2);
//		//m.PasarPila(p1, p2);
//		/*System.out.println("Muestro la pila 1");
//		m.MostrarPila(p1);
//		System.out.println("Vuelvo a mostrar la pila 1");
//		m.MostrarPila(p1);*/
//		/*System.out.println("Test de copiar Pila 1 a Pila 2");
//		m.CopiarPila(p1, p2);
//		System.out.println("pila 1:");
//		m.MostrarPila(p1);
//		System.out.println("Pila 2:");
//		m.MostrarPila(p2);*/
//		float prom=m.PromedioPila(p1);
//		System.out.print("El promedio es: ");
//		System.out.print(prom);
		ConjuntoTA conj1=new ConjuntoTA();
		ConjuntoTA conj2=new ConjuntoTA();
		ConjuntoTA unido=new ConjuntoTA();

		conj1.InicializarConjunto();
		conj2.InicializarConjunto();
		unido.InicializarConjunto();

		conj1.Agregar(1);
		conj1.Agregar(2);
		conj1.Agregar(3);
		conj2.Agregar(3);
		conj2.Agregar(4);
		conj2.Agregar(5);
		
		unido=m.Union(conj1, conj2);
		
		while(!unido.ConjuntoVacio()) {
			int elem = unido.Elegir();
			System.out.println(elem);
			unido.Sacar(elem);
			
		}

	}

}
